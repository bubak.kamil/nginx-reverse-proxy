# NGINX Reverse Proxy

## Prerequisites:

- [Minikube](https://kubernetes.io/esen/docs/tasks/tools/install-minikube/#instalar-minikube)
- [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/)
- [Helm](https://helm.sh/)

## Steps:

- Start Minikube

  ```powershell
  $ minikube start
  ```

- Create minikube namespaces:

  ```powershell
  $ kubectl apply -f values.yaml
  ```

- Deploy Google Hello World test apps:

  ```powershell
  $ kubectl apply -f app.yaml -n minikube-app
  ```

- Apply app configuration file:

  ```powershell
  $ kubectl apply -f port.yaml -n minikube-app
  ```

- Install nginx using helm:

  ```powershell
  $ helm install my-nginx -n minikube-proxy bitnami/nginx
  ```

- Edit values.yaml:

  ```powershell
  serverBlock: |-
    upstream backend {
          server 0.0.0.0:8080; # app address
          server 0.0.0.0:8080; # app address
          server 0.0.0.0:8080; # app address
    }
    server {
        listen 8080;
        location / {
            proxy_pass http://backend;
        }
    }
  ```

- You can find app address by typing:

  ```powershell
  $ kubectl get pods -o wide -n minikube-app
  ```

- Apply configuration file:

  ```powershell
  $ kubectl apply -f values.yaml -n minikube-proxy
  ```

- Start NGINX service:

  ```powershell
  $ minikube service my-nginx
  ```

## Sources

- [Bitnami NGINX](https://github.com/bitnami/charts/tree/master/bitnami/nginx) repository
- [Kubernetes](https://kubernetes.io/) documentation
- [Helm](https://helm.sh/) documentation